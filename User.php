<?php
require_once("autoload.php");
class User extends Connection {
    private $name;
    private $phone_num; 
    private $email; 
    private $conn;
 
        public function __construct()
        {
            $this->conn = new Connection();
            $this->conn = $this->conn->connection();
        }

        public function insertUser(string $strName,string $strPhone,string $strEmail){

            $this->name = $strName;
            $this->phone_num = $strPhone;
            $this->email = $strEmail;

            $sql = "INSERT INTO user(name,phone_num,email) VALUES(?,?,?)";
            $insert = $this->conn->prepare($sql);
            $arrData = array($this->name,$this->phone_num,$this->email);
            
            $resInsert = $insert->execute($arrData);
            $lastId = $insert->conn->lastInsertId();
            return $lastid;
        }
        public function updateUser(int $id, string $strName,string $strPhone,string $strEmail){

            $this->name = $strName;
            $this->phone_num = $strPhone;
            $this->email = $strEmail;

           $sql = "UPDATE user set name=?, phone_num=?, email=? WHERE id=$id";

           $update = $this->conn->prepare($sql);

           $arrData = array($this->name,$this->phone_num,$this->email);

           $resUpdate = $update->execute($arrData);

           return $resUpdate;
        }

        public function getUpdate(int $id)
        {
            $sql = "SELECT * FROM user WHERE id = ?";

            $query = $this->conn->prepare($sql);

            $arrWhere = array($id);
            
            $query->execute($arrWhere);

            $request = $query->fetch(PDO::FETCH_ASSOC);
            
            return $request;

        }
        public function deleteUser(int $id)
        {
            $sql = "DELETE * FROM user WHERE id = ?";

            $query = $this->conn->prepare($sql);

            $arrWhere = array($id);
            
            $query->execute($arrWhere);

            $request = $delete->fetch(PDO::FETCH_ASSOC);
            
            return $del;

        }



}